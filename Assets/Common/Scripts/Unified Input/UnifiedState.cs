﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct UnifiedPadState
{
	public Buttons Buttons;

	public float ThumbLeftX, ThumbLeftY, ThumbRightX, ThumbRightY;

	public POVDirections POV;
}
