﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-1)]
public class UnifiedInputBehaviour : MonoBehaviour {
	void Update()
	{
		UnifiedInput.Update();
	}

	void OnApplicationQuit()
	{
		UnifiedInput.Dispose();
	}
}
