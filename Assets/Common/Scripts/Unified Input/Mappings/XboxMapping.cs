﻿using SharpDX.DirectInput;
using System;

/// <summary>
/// Xbox Controller for Windows用のマッピング。
/// 実はXboxコンのエミュしかテストしておらず、実際のXboxコンは動作未確認。
/// </summary>
public class XboxMapping : Mapping
{
	public override Guid TargetProductGuid => new Guid("028e045e-0000-0000-0000-504944564944");

	public override UnifiedPadState Convert(JoystickState state)
	{
		var ret = new UnifiedPadState();

		DefaultButtonMapping(state, ref ret);

		ret.ThumbLeftX = state.X / 32768f - 1;
		ret.ThumbLeftY = -state.Y / 32768f + 1;
		ret.ThumbRightX = state.RotationX / 32768f - 1;
		ret.ThumbRightY = -state.RotationY / 32768f + 1;

		DefaultPovMapping(state, ref ret);

		return ret;
	}
}
