﻿using SharpDX.DirectInput;
using System;

/// <summary>
/// ELECOM FU2912F用のマッピング。
/// 光沢があり人間工学的なデザインのパッド。
/// </summary>
public class ElecomFU2912FMapping : Mapping
{
	public override Guid TargetProductGuid => new Guid("333111ff-0000-0000-0000-504944564944");

	static readonly Buttons[] _buttonMapping = {
		Buttons.X,
		Buttons.Y,
		Buttons.A,
		Buttons.B,
		Buttons.LButton,
		Buttons.RButton,
		Buttons.LTrigger,
		Buttons.RTrigger,
		Buttons.LThumb,
		Buttons.RThumb,
		Buttons.Back,
		Buttons.Start
	};

	public override UnifiedPadState Convert(JoystickState state)
	{
		var ret = new UnifiedPadState();

		for (int i = 0; i < state.Buttons.Length; i++)
		{
			if (state.Buttons[i])
				ret.Buttons |= _buttonMapping[i];
		}

		DefaultThumbMapping(state, ref ret);

		DefaultPovMapping(state, ref ret);

		return ret;
	}
}
