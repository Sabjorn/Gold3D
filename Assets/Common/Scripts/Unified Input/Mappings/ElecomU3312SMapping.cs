﻿using SharpDX.DirectInput;
using System;

/// <summary>
/// ELECOM U3312S用のマッピング。
/// 光沢がなく円をモチーフとしたデザインのパッド。
/// </summary>
public class ElecomU3312SMapping : Mapping
{
	public override Guid TargetProductGuid => new Guid("100405b8-0000-0000-0000-504944564944");

	static readonly Buttons[] _buttonMapping = {
		Buttons.X,
		Buttons.Y,
		Buttons.A,
		Buttons.B,
		Buttons.LButton,
		Buttons.RButton,
		Buttons.LTrigger,
		Buttons.RTrigger,
		Buttons.Back,
		Buttons.Start,
		Buttons.LThumb,
		Buttons.RThumb
	};

	public override UnifiedPadState Convert(JoystickState state)
	{
		var ret = new UnifiedPadState();

		for (int i = 0; i < state.Buttons.Length; i++)
		{
			if (state.Buttons[i])
				ret.Buttons |= _buttonMapping[i];
		}

		DefaultThumbMapping(state, ref ret);

		DefaultPovMapping(state, ref ret);

		return ret;
	}
}
