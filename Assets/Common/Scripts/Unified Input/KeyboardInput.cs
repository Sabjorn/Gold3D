﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SharpDX.DirectInput;
using UnityEngine;

/// <summary>
/// 統合されたパッドを表すクラス。
/// </summary>
public class KeyboardInput : UnifiedPad
{
	
	/// <summary>
	/// パッドが有効かどうかを示す。
	/// ※重い処理のためメインループなどで使用しない。
	/// </summary>
	public override bool IsAvailable => true;

	/// <summary>
	/// パッドのインスタンスID。
	/// </summary>
	public override Guid InstanceGuid => Guid.Empty;

	/// <summary>
	/// パッドの製品ID。
	/// </summary>
	public override Guid ProductGuid => Guid.Empty;

	/// <summary>
	/// パッドの製品名。
	/// </summary>
	public override string ProductName => "keyboard";


	/// <summary>
	/// キーボードをunifiedPadとして初期化
	/// </summary>
	public KeyboardInput()
	{
		
	}

	/// <summary>
	/// 入力状態を更新する。
	/// </summary>
	public override void Update()
	{
		_padState.ThumbLeftX = Input.GetKey(KeyCode.A) ? -1.0f : Input.GetKey(KeyCode.D) ? 1.0f : 0.0f;
		_padState.ThumbLeftY = Input.GetKey(KeyCode.S) ? -1.0f : Input.GetKey(KeyCode.W) ? 1.0f : 0.0f;
		_padState.ThumbRightX = Input.GetAxis("Horizontal");
		_padState.ThumbRightY = Input.GetAxis("Vertical");

		_padState.Buttons = 0;
		_padState.Buttons |= Input.GetKey(KeyCode.Space) ? Buttons.A : 0;
		_padState.Buttons |= Input.GetKey(KeyCode.Q) ? Buttons.B : 0;
		_padState.Buttons |= Input.GetKey(KeyCode.E) ? Buttons.X : 0;
		_padState.Buttons |= Input.GetKey(KeyCode.R) ? Buttons.Y : 0;
    _padState.Buttons |= Input.GetKey(KeyCode.C) ? Buttons.RTrigger : 0;
	}

	/// <summary>
	/// パッドを破棄する。
	/// </summary>
	public override void Dispose()
	{
		
	}
}
