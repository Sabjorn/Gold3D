﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SharpDX.DirectInput;
using UnityEngine;

/// <summary>
/// 統合されたパッドを表すクラス。
/// </summary>
public class UnifiedPad : IDisposable
{
	private readonly DirectInput _directInput;
	private readonly Joystick _pad;
	private readonly Mapping _mapping;

	/// <summary>
	/// パッドが有効かどうかを示す。
	/// ※重い処理のためメインループなどで使用しない。
	/// </summary>
	public virtual bool IsAvailable => !_pad.IsDisposed && _directInput.IsDeviceAttached(_pad.Information.InstanceGuid);

	/// <summary>
	/// パッドのインスタンスID。
	/// </summary>
	public virtual Guid InstanceGuid { get; }

	/// <summary>
	/// パッドの製品ID。
	/// </summary>
	public virtual Guid ProductGuid { get; }

	/// <summary>
	/// パッドの製品名。
	/// </summary>
	public virtual string ProductName { get; }

	protected UnifiedPadState _padState;
	/// <summary>
	/// パッドの現在の入力状態。
	/// </summary>
	public UnifiedPadState State => _padState;

	public float DeadZone { get; set; }

	private UnifiedPadState _rawPadState;

	private readonly UnifiedPadState _neutralPadState = new UnifiedPadState();

	private JoystickState _state = new JoystickState();

	public UnifiedPad() { }
	/// <summary>
	/// DirectInputのJoystickクラスから新しいUnifiedPadインスタンスを生成する。
	/// </summary>
	/// <param name="directInput">DirectInputコア。</param>
	/// <param name="pad">使用するJoystick。</param>
	/// <param name="mapping">Joystick入力をUnifiedStateに変換するためのマップ。</param>
	public UnifiedPad(DirectInput directInput, Joystick pad, Mapping mapping)
	{
		_directInput = directInput;
		_pad = pad;
		_mapping = mapping;

		InstanceGuid = _pad.Information.InstanceGuid;
		ProductGuid = _pad.Information.ProductGuid;
		ProductName = _pad.Information.ProductName;

		DeadZone = 0.03f;
	}

	/// <summary>
	/// 入力状態を更新する。
	/// </summary>
	public virtual void Update()
	{
		try
		{
			_pad.GetCurrentState(ref _state);
			_rawPadState = _mapping.Convert(_state);

			_padState = _rawPadState;

			// 遊び処理
			_padState.ThumbLeftX = Mathf.Abs(_padState.ThumbLeftX) >= DeadZone ? _padState.ThumbLeftX : 0;
			_padState.ThumbLeftY = Mathf.Abs(_padState.ThumbLeftY) >= DeadZone ? _padState.ThumbLeftY : 0;
			_padState.ThumbRightX = Mathf.Abs(_padState.ThumbRightX) >= DeadZone ? _padState.ThumbRightX : 0;
			_padState.ThumbRightY = Mathf.Abs(_padState.ThumbRightY) >= DeadZone ? _padState.ThumbRightY : 0;
		}
		catch (Exception e)
		{
			_padState = _neutralPadState;
			Debug.LogWarning($"このゲームパッドは無効です。ゲームパッドを取得し直してください。入力状態はニュートラルに設定されます。\nパッド名: {ProductName}\nエラー: {e.Message}");
		}
	}

	/// <summary>
	/// パッドを破棄する。
	/// </summary>
	public virtual void Dispose()
	{
		_pad.Dispose();
	}
}
