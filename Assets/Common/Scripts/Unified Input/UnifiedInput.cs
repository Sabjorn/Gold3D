﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using SharpDX.DirectInput;
using UnityEngine;
using Object = UnityEngine.Object;

/// <summary>
/// 統合されたゲームパッド入力管理クラス。
/// </summary>
public static class UnifiedInput
{
	/// <summary>
	/// DirectInputコア。
	/// </summary>
	private static readonly DirectInput _directInput;

	/// <summary>
	/// デバイス監視タスク。
	/// </summary>
	private static readonly Task _deviceChangesWatching;

	/// <summary>
	/// デバイス監視タスクのキャンセルトークン。
	/// </summary>
	private static readonly CancellationTokenSource _cancelTokenSource;

	/// <summary>
	/// 読み込まれたマッピング。
	/// </summary>
	private static readonly List<Mapping> _mappings;

	/// <summary>
	/// 現在接続されているゲームパッドのリスト。
	/// </summary>
	public static readonly List<UnifiedPad> Pads = new List<UnifiedPad>();

	/// <summary>
	/// UnityとのブリッジとなるGameObject。
	/// </summary>
	private static readonly GameObject _gameObject;

	static UnifiedInput ()
	{
		// マッピング読み込み
		_mappings = Assembly.GetAssembly (typeof(Mapping))
			.GetTypes ()
			.Where (t => !t.IsAbstract && t.IsSubclassOf (typeof(Mapping)))
			.Select (t => (Mapping)Activator.CreateInstance (t))
			.ToList ();

		// keyboard追加
		//if (Environment.OSVersion.Platform == PlatformID.Unix) {
			Pads.Add (new KeyboardInput ());
		//}

		if (Environment.OSVersion.Platform == PlatformID.Win32NT) {
			// DirectInput初期化
			_directInput = new DirectInput ();


			// デバイス監視開始
			_cancelTokenSource = new CancellationTokenSource ();
			_deviceChangesWatching = Task.Run (() => {
				while (true) {
					// 接続が解除されたパッドの削除
					lock (Pads) {
						foreach (var pad in Pads) {
							if (!pad.IsAvailable) {
								Debug.Log ($"ゲームパッド接続解除: {pad.ProductName}");
								pad.Dispose ();
							}
						}

						Pads.RemoveAll (pad => !pad.IsAvailable);
					}

					// 新たに接続されたパッドの追加
					var devices = _directInput.GetDevices (DeviceClass.GameControl, DeviceEnumerationFlags.AttachedOnly);
					lock (Pads) {
						foreach (var device in devices) {
							if (Pads.Exists (p => p.InstanceGuid == device.InstanceGuid))
								continue;

							Debug.Log ($"ゲームパッド新規接続: {device.ProductName}");

							var pad = new Joystick (_directInput, device.InstanceGuid);
							pad.Acquire ();

							Mapping mapping = new Mapping ();
							if (_mappings.Exists (m => m.TargetProductGuid == device.ProductGuid)) {
								mapping = _mappings.First (m => m.TargetProductGuid == device.ProductGuid);
								Debug.Log ($"マッピングが見つかりました: {mapping}");
							} else {
								Debug.LogWarning ("マッピングが見つかりませんでした。デフォルトのマッピングが使用されます。");
							}

							Pads.Add (new UnifiedPad (_directInput, pad, mapping));
						}
					}

					// キャンセル時に終了
					if (_cancelTokenSource.Token.IsCancellationRequested)
						return;
				}
			}, _cancelTokenSource.Token);
		}

		// UnityブリッジGameObject生成
		_gameObject = new GameObject("Unified Input Handler");
		_gameObject.AddComponent<UnifiedInputBehaviour>();
		Object.DontDestroyOnLoad(_gameObject);
	}

	/// <summary>
	/// すべてのパッドの状態を更新する。
	/// </summary>
	public static void Update()
	{
		lock (Pads)
		{
			foreach (var pad in Pads)
			{
				pad.Update();
			}
		}
	}

	/// <summary>
	/// UnifiedInputを終了する。
	/// </summary>
	public static void Dispose()
	{
		_cancelTokenSource.Cancel();
		_deviceChangesWatching.Wait();

		_directInput.Dispose();
	}
}
