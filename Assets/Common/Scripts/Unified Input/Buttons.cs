﻿using System;

[Flags]
public enum Buttons : uint
{
	None = 0,
	A = 1 << 0,
	B = 1 << 1,
	X = 1 << 2,
	Y = 1 << 3,
	LButton = 1 << 4,
	RButton = 1 << 5,
	Back = 1 << 6,
	Start = 1 << 7,
	LThumb = 1 << 8,
	RThumb = 1 << 9,
	LTrigger = 1 << 10,
	RTrigger = 1 << 11
}
