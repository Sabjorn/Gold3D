﻿using System;
using SharpDX.DirectInput;

/// <summary>
/// ゲームパッドごとの差異を吸収してUnifiedPadStateに統合するためのマッピングを行うクラス。
/// パッドごとにこのクラスを継承する。
/// </summary>
public class Mapping
{
	/// <summary>
	/// マッピングを適用する製品ID。
	/// </summary>
	public virtual Guid TargetProductGuid => Guid.Empty;

	/// <summary>
	/// JoystickStateをUnifiedStateに変換する。
	/// </summary>
	/// <param name="state">変換元のJoystickState。</param>
	/// <returns>変換されたUnifiedPadState。</returns>
	public virtual UnifiedPadState Convert(JoystickState state)
	{
		var ret = new UnifiedPadState();

		DefaultButtonMapping(state, ref ret);

		DefaultThumbMapping(state, ref ret);

		DefaultPovMapping(state, ref ret);

		return ret;
	}

	/// <summary>
	/// ボタン入力をデフォルト設定を使用して変換する。
	/// </summary>
	/// <param name="state">変換元のJoystickState。</param>
	/// <param name="padState">変換先のUnifiedPadState。</param>
	public static void DefaultButtonMapping(JoystickState state, ref UnifiedPadState padState)
	{
		for (int i = 0; i < state.Buttons.Length; i++)
		{
			if (state.Buttons[i])
				padState.Buttons |= (Buttons)(1 << i);
		}
	}

	/// <summary>
	/// スティック入力をデフォルト設定を使用して変換する。
	/// </summary>
	/// <param name="state">変換元のJoystickState。</param>
	/// <param name="padState">変換先のUnifiedPadState。</param>
	public static void DefaultThumbMapping(JoystickState state, ref UnifiedPadState padState)
	{
		padState.ThumbLeftX = state.X / 32768f - 1;
		padState.ThumbLeftY = -state.Y / 32768f + 1;
		padState.ThumbRightX = state.Z / 32768f - 1;
		padState.ThumbRightY = -state.RotationZ / 32768f + 1;
	}

	/// <summary>
	/// ハットスイッチ入力をデフォルト設定を使用して変換する。
	/// </summary>
	/// <param name="state">変換元のJoystickState。</param>
	/// <param name="padState">変換先のUnifiedPadState。</param>
	public static void DefaultPovMapping(JoystickState state, ref UnifiedPadState padState)
	{
		switch (state.PointOfViewControllers[0])
		{
			case -1:
				padState.POV = POVDirections.None;
				break;
			case 0:
				padState.POV = POVDirections.Up;
				break;
			case 4500:
				padState.POV = POVDirections.Up | POVDirections.Right;
				break;
			case 9000:
				padState.POV = POVDirections.Right;
				break;
			case 13500:
				padState.POV = POVDirections.Right | POVDirections.Down;
				break;
			case 18000:
				padState.POV = POVDirections.Down;
				break;
			case 22500:
				padState.POV = POVDirections.Down | POVDirections.Left;
				break;
			case 27000:
				padState.POV = POVDirections.Left;
				break;
			case 31500:
				padState.POV = POVDirections.Left | POVDirections.Up;
				break;
			default:
				padState.POV = POVDirections.None;
				break;
		}
	}
}
