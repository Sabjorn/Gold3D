﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngineInternal.Input;

[RequireComponent(typeof(Rigidbody))]
[NetworkSettings(sendInterval = 0.03f, channel = 1)]
public class NetworkSyncTransform : NetworkBehaviour
{
	public bool Upload = true, Download = true;

	[SyncVar] private Vector3 _position = Vector3.zero, _velocity = Vector3.zero;
	[SyncVar] private Quaternion _rotation = Quaternion.identity;

	private NetworkIdentity _identity;
	private Rigidbody _rigidbody;

	void Awake()
	{
		_identity = GetComponent<NetworkIdentity>();
		_rigidbody = GetComponent<Rigidbody>();
	}

	void Start()
	{
		// クライアント処理
		if (isClient)
			Observable.Interval(TimeSpan.FromSeconds(this.GetNetworkSendInterval()))
			.Subscribe(_ =>
			{
				if (hasAuthority)
				{
					if (Upload)
						CmdSendStatus(_rigidbody.position, _rigidbody.velocity, transform.rotation);
				}
				else
				{
					if (Download)
					{
						_rigidbody.position = _position;
						_rigidbody.velocity = _velocity;
						transform.rotation = _rotation;
					}
				}
			}).AddTo(gameObject);
	}

	void Update()
	{
		// サーバ処理
		if (isServer)
		{
			var owner = _identity.clientAuthorityOwner;//?.playerControllers[0].unetView;

			// 誰も所有していない場合
			if (owner == null)
			{
				// 変数をサーバの状態に更新する
				_position = _rigidbody.position;
				_velocity = _rigidbody.velocity;
				_rotation = transform.rotation;
			}
		}
	}

	[Command(channel = 1)]
	void CmdSendStatus(Vector3 position, Vector3 velocity, Quaternion rotation)
	{
		_position = position;
		_velocity = velocity;
		_rotation = rotation;

		// サーバの状態を変数に合わせる
		_rigidbody.position = _position;
		_rigidbody.velocity = _velocity;
		transform.rotation = _rotation;
	}
}
