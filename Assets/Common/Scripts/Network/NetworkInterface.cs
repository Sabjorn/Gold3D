﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(NetworkManager))]
public class NetworkInterface : MonoBehaviour
{
	private NetworkManager _networkManager;

	void Awake()
	{
		_networkManager = GetComponent<NetworkManager>();

		// Linuxでサーバ自動起動
#if UNITY_STANDALONE_LINUX
		_networkManager.StartServer();
#endif
	}

	public void StartHost()
	{
		_networkManager.StartHost();
	}

	public void StartClient()
	{
		_networkManager.StartClient();
	}

	public void StartServer()
	{
		_networkManager.StartServer();
	}
}
