﻿using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class InputTest : MonoBehaviour
{
	public Text LogText;

	void Update()
	{
		var stringBuilder = new StringBuilder();

		stringBuilder.AppendLine("Unified Input:");

		foreach (var pad in UnifiedInput.Pads)
		{
			stringBuilder.AppendLine($"\tProduct Name: {pad.ProductName}");
			stringBuilder.AppendLine($"\tProduct GUID: {pad.ProductGuid}");
			stringBuilder.AppendLine($"\tInstance GUID: {pad.InstanceGuid}");
			stringBuilder.AppendLine($"\tButtons: {pad.State.Buttons}");
			stringBuilder.AppendLine($"\tLThumb: {pad.State.ThumbLeftX:+0.000; 0.000;-0.000} : {pad.State.ThumbLeftY:+0.000; 0.000;-0.000}");
			stringBuilder.AppendLine($"\tRThumb: {pad.State.ThumbRightX:+0.000; 0.000;-0.000} : {pad.State.ThumbRightY:+0.000; 0.000;-0.000}");
			stringBuilder.AppendLine($"\tPOV: {pad.State.POV}");
			stringBuilder.AppendLine("");
		}

		LogText.text = stringBuilder.ToString();
	}
}
