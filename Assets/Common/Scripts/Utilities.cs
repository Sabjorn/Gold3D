using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace Utilities
{
	public static class Particle
	{
		public static ParticleSystem InstantiateParticle(ParticleSystem particlePrefab, Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion))
		{
			var instance = Object.Instantiate(particlePrefab).gameObject;
			instance.transform.position = position;
			instance.transform.rotation = rotation;

			var particle = instance.GetComponent<ParticleSystem>();
			particle.Play();
			instance.UpdateAsObservable()
				.Where(_ => !particle.IsAlive(true))
				.First()
				.Subscribe(_ =>
				{
					Object.Destroy(instance);
				})
				.AddTo(instance);

			return particle;
		}
	}

}