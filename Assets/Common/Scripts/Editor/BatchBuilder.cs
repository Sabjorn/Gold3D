﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;

public class BatchBuilder
{
    public static void Build()
    {
        var scenes = EditorBuildSettings.scenes;
        var outPath = Path.GetDirectoryName(Application.dataPath) + "\\Build\\Linux\\Gold";

        Console.WriteLine("Included Scenes:");
        Console.ForegroundColor = ConsoleColor.Yellow;
        foreach (var scene in scenes)
        {
            Console.WriteLine(scene);
        }
        Console.ResetColor();

        Console.WriteLine("Output: ");
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine(outPath);
        Console.ResetColor();

        Console.WriteLine("Building...");
        var report = BuildPipeline.BuildPlayer(
                scenes,
                outPath,
                BuildTarget.StandaloneLinux64,
                BuildOptions.None
            );

        if (report.summary.result != UnityEditor.Build.Reporting.BuildResult.Succeeded)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("*** Error! ***");
            Console.ResetColor();
            return;
        }
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Done!");
        Console.ResetColor();
    }
}
