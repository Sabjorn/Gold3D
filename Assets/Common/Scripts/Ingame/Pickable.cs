﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Networking;

public class Pickable : NetworkBehaviour
{
	[SyncVar] public GameObject Owner;
	[SyncVar] public GameObject AssignedZone;

	public Collider PickTrigger, PhysicalCollider;

	private Rigidbody _rigidbody;
	private NetworkSyncTransform _networkSyncTransform;

	void Awake()
	{
		_rigidbody = GetComponent<Rigidbody>();
		_networkSyncTransform = GetComponent<NetworkSyncTransform>();

		PhysicalCollider.OnTriggerStayAsObservable()
			.Select(c => c.gameObject.GetComponent<Zone>())
			.Where(z => z != null)
			.Subscribe(zone =>
			{
				if (Owner == null)
				{
					AssignedZone = zone.gameObject;
				}
			});
	}

	void Update()
	{
		if (Owner != null)
		{
			_networkSyncTransform.Download = false;

			_rigidbody.useGravity = false;

			PickTrigger.enabled = false;
			PhysicalCollider.enabled = false;

			_rigidbody.velocity =
				10f * (Owner.GetComponent<Picker>().StorageTransform.position - _rigidbody.position);
		}
		else
		{
			_networkSyncTransform.Download = true;

			_rigidbody.useGravity = true;

			PickTrigger.enabled = true;
			PhysicalCollider.enabled = true;
			if (isServer && AssignedZone != null)
			{
				_rigidbody.velocity = 1f * (AssignedZone.transform.position - _rigidbody.position);
			}
		}
	}

	public void AssignZone(GameObject zone)
	{
		CmdAssignZone(zone);
	}

	[Command]
	public void CmdAssignZone(GameObject zone)
	{
		AssignedZone = zone;
	}
}
