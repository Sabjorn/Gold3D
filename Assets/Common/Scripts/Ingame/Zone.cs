﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Networking;

class GameObjectList : SyncList<GameObject>
{
	protected override void SerializeItem(NetworkWriter writer, GameObject item)
	{
		writer.Write(item);
	}

	protected override GameObject DeserializeItem(NetworkReader reader)
	{
		return reader.ReadGameObject();
	}
}

public class Zone : NetworkBehaviour
{
	public int TeamId = 0;
	public float Score = 0;

	[SyncVar]
	private readonly GameObjectList capturedGolds = new GameObjectList();
	private readonly List<GameObject> capturedGoldsBuffer = new List<GameObject>();

	private TextMesh textMesh;

	void Awake()
	{
		textMesh = GetComponentInChildren<TextMesh>();
	}

	void Start()
	{
		this.OnTriggerStayAsObservable()
			.Select(c => c.GetComponent<Pickable>())
			.Where(c => c != null)
			.Subscribe(gold =>
			{
				if (isServer)
					if (!capturedGoldsBuffer.Contains(gold.gameObject)) capturedGoldsBuffer.Add(gold.gameObject);
			});
		this.LateUpdateAsObservable()
			.Subscribe(_ =>
			{
				if (isServer)
				{
					capturedGolds.Clear();
					foreach (var c in capturedGoldsBuffer)
					{
						capturedGolds.Add(c);
					}
					capturedGoldsBuffer.Clear();
				}
			});
	}

	void Update()
	{
		if (textMesh != null)
			textMesh.text = $"Gold: {capturedGolds.Count}";
	}
}
