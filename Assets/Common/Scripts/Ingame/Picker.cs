﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Networking;

public class Picker : NetworkBehaviour
{
	public Collider SearchTrigger;
	public Transform StorageTransform;
	private CharacterMovement parent;

	public Pickable CurrentPickable { get; private set; }

	private IDisposable _searchObservable;
	private IDisposable _pickedFollowObservable;

	private readonly List<Pickable> _grabbableCandidates = new List<Pickable>();

	void Start()
	{
		parent = GetComponentInParent<CharacterMovement>();
		_searchObservable = new CompositeDisposable(
			this.OnCollisionStayAsObservable()
				.Select(c => c.gameObject.GetComponent<Pickable>())
			.Where(g => g != null && (g.AssignedZone == null || g.AssignedZone.GetComponent<Zone>().TeamId != parent.TeamId))
				.Subscribe(g => { _grabbableCandidates.Add(g); }),
			this.LateUpdateAsObservable()
				.DoOnCancel(() => { })
			.Subscribe(_ =>
				{
					_grabbableCandidates.Clear();
				})
		).AddTo(gameObject);
	}

	public bool Pick()
	{
		if (CurrentPickable != null) return false;

		if (!_grabbableCandidates.Any()) return false;

		CurrentPickable = _grabbableCandidates.First();
		CmdRequestOwnership(CurrentPickable.gameObject);

		return true;
	}

	public bool Release()
	{
		if (CurrentPickable == null) return false;

		CmdReleaseOwnership(CurrentPickable.gameObject);
		CurrentPickable = null;

		return true;
	}

	[Command]
	void CmdRequestOwnership(GameObject pickable)
	{
		var _pickable = pickable.GetComponent<Pickable>();

		if (_pickable.Owner != null)
		{
			TargetRequestOwnershipResult(connectionToClient, false);
			return;
		}

		pickable.GetComponent<NetworkIdentity>().AssignClientAuthority(connectionToClient);
		_pickable.Owner = gameObject;
		TargetRequestOwnershipResult(connectionToClient, true);
	}

	[TargetRpc]
	void TargetRequestOwnershipResult(NetworkConnection target, bool result)
	{
		if(result)
			CurrentPickable.GetComponent<Pickable>().AssignZone(null);
	}

	[Command]
	void CmdReleaseOwnership(GameObject pickable)
	{
		var _pickable = pickable.GetComponent<Pickable>();

		pickable.GetComponent<NetworkIdentity>().RemoveClientAuthority(connectionToClient);
		_pickable.Owner = null;
		pickable.GetComponent<Rigidbody>().velocity = transform.rotation * Vector3.forward * -10f;
	}
}
