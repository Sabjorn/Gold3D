﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;

public class ThirdPersonCamera : MonoBehaviour
{
	public CharacterMovement character;

	public Vector3 PositionOffset;
	public Vector3 RotationOffset;

	void Start()
	{
		Observable.Interval(TimeSpan.FromSeconds(1.0))
			.Subscribe(_ =>
			{
				try
				{
					var localPlayer = FindObjectsOfType<CharacterMovement>()
						.First(c => c.GetComponent<NetworkIdentity>().isLocalPlayer);

					character = localPlayer;
				}
				catch (Exception e) { }
			});
	}

	private void Update()
	{
		if (character != null)
		{
			var q = Quaternion.LookRotation(character.TargetDirection, Vector3.up);

			transform.position = character.transform.position + q * PositionOffset;
			transform.localRotation = q * Quaternion.Euler(RotationOffset);
		}
	}
}
