﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

[Flags]
public enum SkillTypes : uint
{
	None = 0,
	Active = 1 << 0,
	Passive = 1 << 1,
	Debuf = 1 << 2
}

public class SkillSystem
{
	List<Tuple<Skill, IDisposable>> _skills = new List<Tuple<Skill, IDisposable>>();

	public void AddSkill(Skill skill, bool ignoreWhenDuplicate = true)
	{
		if (_skills.Exists(s => s.Item1 == skill))
		{
			Debug.Log("Duplication");

			if (ignoreWhenDuplicate)
				return;

			RemoveSkill(_s => _s == skill);
		}

		var t = new Tuple<Skill, IDisposable>(skill, skill.Sequence.DelayFrame(1).Subscribe(_ => RemoveSkill(s => s == skill)));
		_skills.Add(t);
	}

	public void RemoveSkill(Func<Skill, bool> pred)
	{
		var rem = new List<Tuple<Skill, IDisposable>>();

		foreach (var tuple in _skills.Where(t => pred(t.Item1)))
		{
			tuple.Item2.Dispose();
			tuple.Item1.OnCancel();

			rem.Add(tuple);
		}

		_skills.RemoveAll(t => rem.Contains(t));
	}
}

public class Skill
{
	public string Name = "";
	public SkillTypes Types = SkillTypes.None;
	public IObservable<Unit> Sequence = Observable.ReturnUnit();
	public Action OnCancel = () => { };
}