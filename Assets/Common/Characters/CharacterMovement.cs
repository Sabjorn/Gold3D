﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

/// <summary>
/// キャラクターの操作を定義するBehaviour。
/// </summary>
[RequireComponent(typeof(Picker))]
public class CharacterMovement : NetworkBehaviour
{
	public Collider FootCollider;
	[SerializeField] private Collider _zoneEnterCollider;
	public float Velocity, Acceleration, JumpVelocity, LookSensitivity;

	[HideInInspector]
	public Vector3 TargetVelocity;
	[HideInInspector]
	public Vector3 TargetDirection = Vector3.forward;

	public bool IsControllable = true;

	protected Animator animator;

	protected bool isGround;
	private List<Collider> _groundCandidates = new List<Collider>();
	protected NetworkIdentity _networkIdentity;
	protected NetworkAnimator _networkAnimator;

	protected UnifiedPad pad;
	protected new Rigidbody rigidbody;

	protected SkillSystem skillSystem;

	protected Picker _picker;

	protected Buttons _prevButtons;

	public CooldownSession[] CooldownSessions;
	public Slider[] CooldownGauges;

	public int TeamId = 0;

	protected void Awake()
	{
		CooldownSessions = null;
		CooldownGauges[0] = GameObject.Find("Skill1CD").GetComponent<Slider>();
		CooldownGauges[1] = GameObject.Find("Skill2CD").GetComponent<Slider>();
		TeamId = FindObjectsOfType<CharacterMovement>().Length - 1;

		skillSystem = new SkillSystem();
	}

	protected void Start()
	{
		rigidbody = GetComponent<Rigidbody>();
		animator = GetComponent<Animator>();

		_networkIdentity = GetComponent<NetworkIdentity>();
		_networkAnimator = GetComponent<NetworkAnimator>();

		_picker = GetComponent<Picker>();

		// 接地判定
		FootCollider.OnTriggerEnterAsObservable()
			.Where(col => col.gameObject.tag == "Ground")
			.Subscribe(col =>
			{
				_groundCandidates.Add(col);
				isGround = true;
			});
		FootCollider.OnTriggerExitAsObservable()
			.Where(col => col.gameObject.tag == "Ground")
			.Subscribe(col =>
			{
				_groundCandidates.Remove(col);
				if (_groundCandidates.Count == 0) isGround = false;
			});

		// 自陣リリース判定
		_zoneEnterCollider
			.OnTriggerEnterAsObservable()
			.Where(col => _picker.CurrentPickable != null && col.gameObject.GetComponent<Zone>() != null && col.gameObject.GetComponent<Zone>().TeamId == TeamId)
			.Subscribe(col =>
			{
				_picker.CurrentPickable.AssignZone(FindObjectsOfType<Zone>().First(z => z.TeamId == TeamId).gameObject);
				_picker.Release();
			});

	}

	protected void Update()
	{
		if (isLocalPlayer)
		{
			Control();

			if (pad != null)
				_prevButtons = pad.State.Buttons;

			var turnFactor = Mathf.Lerp(
				0.2f, 1,
				Mathf.Pow(Mathf.InverseLerp(
					-1, 1,
					Vector3.Dot(TargetVelocity, transform.TransformVector(Vector3.forward))
				), 2)
			);
			rigidbody.AddForce((TargetVelocity * turnFactor - Vector3.ProjectOnPlane(rigidbody.velocity, Vector3.up)) * Acceleration,
				ForceMode.VelocityChange);

			if (TargetVelocity.magnitude >= 0.03f)
				transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Vector3.ProjectOnPlane(TargetVelocity, Vector3.up), Vector3.up), 0.2f);

			animator.SetBool("IsGround", isGround);

			animator.SetBool("IsMoving", rigidbody.velocity.magnitude >= 0.1f);
			animator.SetFloat("Speed", rigidbody.velocity.magnitude);

			if (CooldownSessions != null)
			{
				for (int i = 0; i < CooldownSessions.Length; i++)
				{
					CooldownSessions[i].Update();
					CooldownGauges[i].value = 1f - CooldownSessions[i].RemainingTime / CooldownSessions[i].MaxTime;
				}
			}

			if (Input.GetKeyDown(KeyCode.J))
			{
				Stun(3);
			}
		}
	}

	protected virtual void Control()
	{
		if (pad != null)
		{
			var leftThumb = new Vector2(pad.State.ThumbLeftX, pad.State.ThumbLeftY);
			var rightThumb = new Vector2(pad.State.ThumbRightX, pad.State.ThumbRightY);

			TargetDirection = (Quaternion.Euler(0, rightThumb.x * LookSensitivity, 0) * TargetDirection);
			TargetDirection =
			(Quaternion.AngleAxis(rightThumb.y * LookSensitivity, Vector3.Cross(TargetDirection, Vector3.up)) *
			 TargetDirection);

			if (IsControllable)
			{

				TargetVelocity = Quaternion.LookRotation(Vector3.ProjectOnPlane(TargetDirection, Vector3.up), Vector3.up) *
								 (new Vector3(leftThumb.x, 0, leftThumb.y) * Velocity);

				if (isGround && (Buttons.A & (~_prevButtons & pad.State.Buttons)) != 0)
				{
					rigidbody.AddForce(Vector3.up * JumpVelocity, ForceMode.VelocityChange);
					//animator.SetTrigger("Jump");
					SetTrigger("Jump");
				}

				if (_picker.CurrentPickable == null)
					_picker.Pick();
				if ((Buttons.RButton & (~_prevButtons & pad.State.Buttons)) != 0)
					_picker.Release();
			}
		}
		else
		{
			if (UnifiedInput.Pads.Any())
				pad = UnifiedInput.Pads[0];
		}
	}

	private void OnAnimatorIK()
	{
		animator.SetLookAtWeight(1, 0.2f, 1);
		animator.SetLookAtPosition(transform.position + TargetDirection * 100f);
	}

	public void Stun(float duration)
	{
		skillSystem.RemoveSkill(s => (s.Types & SkillTypes.Active) != 0);
		skillSystem.RemoveSkill(s => s.Name == "Stun");
		skillSystem.AddSkill(new Skill()
		{
			Name = "Stun",
			Sequence = Observable.Defer(() =>
				{
					IsControllable = false;
					_picker.Release();
					TargetVelocity = Vector3.zero;

					animator.SetBool("Damaged", true);
					return Observable.Timer(TimeSpan.FromSeconds(duration));
				})
				.Do(_ =>
				{
					IsControllable = true;
					animator.SetBool("Damaged", false);
				})
				.AsUnitObservable(),
			OnCancel = () =>
			{
				IsControllable = true;
				animator.SetBool("Damaged", false);
			}
		}, false);
	}

	[ClientRpc(channel = 2)]
	public void RpcStun(float duration)
	{
		Stun(duration);
	}

	protected void SetTrigger(string trigger)
	{
		_networkAnimator.SetTrigger(trigger);
		//　サーバーとして稼働している場合はNetworkAnimatorのAnimatorのAttackトリガーをリセットし2重の再生を防ぐ
		if (_networkIdentity.isServer)
		{
			_networkAnimator.animator.ResetTrigger(trigger);
		}
	}
}
