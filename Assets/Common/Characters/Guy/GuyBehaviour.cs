﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Networking;

public class GuyBehaviour : CharacterMovement
{
	[Header("スキル1: ダッシュ")]
	public Collider DashTrigger;
	public float DashCoolDown;
	public float DashSpeed;
	public float DashDuration;
	public float DashStunDuration;
	public GameObject DashEffect;

	[Header("スキル2: ハンマー振り下ろし")]
	public Collider StompTrigger;
	public float HammerCoolDown;
	public float HammerStunDuration;
	public ParticleSystem StompParticle;

	private List<CharacterMovement> _stompCandidates = new List<CharacterMovement>();

	private List<CharacterMovement> _dashCandidates = new List<CharacterMovement>();
	private List<CharacterMovement> _dashExcludes = new List<CharacterMovement>();

	private Action OnAfterControl;

	protected new void Awake()
	{
		base.Awake();
		CooldownSessions = new CooldownSession[]
		{
			new CooldownSession(DashCoolDown),
			new CooldownSession(HammerCoolDown)
		};
	}

	protected new void Start()
	{
		base.Start();

		StompTrigger.OnTriggerStayAsObservable()
			.Select(c => c.gameObject.GetComponent<CharacterMovement>())
			.Where(ch => ch != null)
			.Subscribe(ch => { if (!_stompCandidates.Contains(ch)) _stompCandidates.Add(ch); })
			.AddTo(gameObject);
		StompTrigger.LateUpdateAsObservable()
			.Subscribe(_ =>
			{
				_stompCandidates.Clear();
			})
			.AddTo(gameObject);

		DashTrigger.OnTriggerStayAsObservable()
			.Select(c => c.gameObject.GetComponent<CharacterMovement>())
			.Where(ch => ch != null)
			.Subscribe(ch => { if (!_dashCandidates.Contains(ch)) _dashCandidates.Add(ch); })
			.AddTo(gameObject);
		DashTrigger.LateUpdateAsObservable()
			.Subscribe(_ =>
			{
				_dashCandidates.Clear();
			})
			.AddTo(gameObject);
	}

	protected override void Control()
	{
		base.Control();
		OnAfterControl?.Invoke();

		if (IsControllable)
		{
			if (CooldownSessions[0].IsCool && (pad.State.Buttons & Buttons.B) != 0 && (_prevButtons & Buttons.B) == 0)
			{
				Dash();
				CooldownSessions[0].Heat();
			}

			if (CooldownSessions[1].IsCool && (pad.State.Buttons & Buttons.X) != 0 && (_prevButtons & Buttons.X) == 0)
			{
				Stomp();
				CooldownSessions[1].Heat();
			}
		}
	}

	[Command(channel = 2)]
	protected void CmdStun(float duration, GameObject[] stunnees)
	{
		foreach (var ch in stunnees.Select(g => g.GetComponent<CharacterMovement>()))
		{
			ch.RpcStun(duration);
		}
	}

	private void Dash()
	{
		var dir = transform.rotation * Vector3.forward;
		var sequence = Observable.Defer(() =>
			{
				IsControllable = false;
				animator.SetBool("Dashing", true);
				DashEffect.SetActive(true);

				return this.LateUpdateAsObservable();
			})
			.Do(_ =>
			{
				rigidbody.velocity = dir * DashSpeed;
				transform.rotation = Quaternion.LookRotation(dir);

				foreach (var ch in _dashCandidates)
				{
					//if (!_dashExcludes.Contains(ch))
					{
						Debug.Log("Dash Hit");
						CmdStun(DashStunDuration, new[] { ch.gameObject });
					}
				}
			})
			.SkipUntil(Observable.Timer(TimeSpan.FromSeconds(DashDuration)))
			.First()
			.Do(_ =>
			{
				IsControllable = true;
				DashEffect.SetActive(false);
				animator.SetBool("Dashing", false);
			});

		skillSystem.AddSkill(new Skill()
		{
			Sequence = sequence,
			OnCancel = () =>
			{
				IsControllable = true;
				DashEffect.SetActive(false);
				animator.SetBool("Dashing", false);
			},
			Types = SkillTypes.Active
		});
	}

	private void Stomp()
	{
		var sequence = Observable.Defer(() =>
			{
				animator.SetBool("Stomping", true);
				OnAfterControl = () =>
				{
					TargetVelocity = 0.2f * Vector3.Slerp(transform.rotation * Vector3.forward * TargetVelocity.magnitude, TargetVelocity, 0.2f);
					Debug.Log(TargetVelocity.magnitude);
				};
				return this.UpdateAsObservable();
			})
			.SkipWhile(_ => (pad.State.Buttons & Buttons.X) != 0)
			.First()
			.Do(_ =>
			{
				IsControllable = false;
				TargetVelocity = Vector3.zero;
				animator.SetBool("Stomping", false);
				OnAfterControl = null;
				Utilities.Particle.InstantiateParticle(StompParticle, StompTrigger.transform.position);

				Debug.Log("Stomps: " + _stompCandidates.Count);

				CmdStun(HammerStunDuration, _stompCandidates.Select(ch => ch.gameObject).ToArray());
			})
			.SelectMany(_ => Observable.Timer(TimeSpan.FromSeconds(0.4f)))
			.Do(_ => { IsControllable = true; })
			.AsUnitObservable();

		skillSystem.AddSkill(new Skill()
		{
			Sequence = sequence,
			OnCancel = () =>
			{
				IsControllable = true;
				animator.SetBool("Stomping", false);
				OnAfterControl = null;
			},
			Types = SkillTypes.Active
		});
	}
}
