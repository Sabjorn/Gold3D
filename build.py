#!/usr/bin/env python3
import sys
import signal
import subprocess
import pathlib
import shutil
import os
from time import sleep
from collections import deque

signal.signal(signal.SIGTERM,
              lambda signal, frame:
              print("SIGTERM!")
              )

# region Constants
RESET = '\033[0m'
RED = '\033[91m'
YELLOW = '\033[93m'
GREEN = '\033[92m'
GRAY = '\033[90m'

MON_HEIGHT = 10
# endregion

unity_path = None

print("Searching Unity as a Program...")
if pathlib.Path('C:/Program Files/Unity/Editor/Unity.exe').exists():
    unity_path = 'C:/Program Files/Unity/Editor/Unity.exe'
elif pathlib.Path('C:/Program Files/Unity/Hub/Editor').exists():
    unityhub_paths = list(pathlib.Path(
        'C:/Program Files/Unity/Hub/Editor').iterdir())
    if len(unityhub_paths) > 0:
        unityhub_paths.sort(reverse=True)
        unity_path = str(unityhub_paths[0].joinpath('./Editor/Unity.exe'))

if unity_path is not None:
    print(GREEN+"Found Unity."+RESET)
else:
    print("Can't find Unity as a Program.")
    print("Checking Unity as a command 'unity'...")
    unity_path = shutil.which('unity')
    if unity_path is None:
        while True:
            p = pathlib.Path(str(unity_path))
            if (p.exists() and p.is_file()):
                break

if unity_path is None:
    print("Can't find Unity. Set a PATH or type the path manually: ", end='')
    unity_path = input()

log_path = str((pathlib.Path(__file__).parent.joinpath(
    './build.log')).absolute())
output_path = str(
    (pathlib.Path(__file__).parent.joinpath('./Build/Linux/Gold').absolute()))

print(('Unity Path: '+YELLOW+'{}'+RESET).format(unity_path))
print(('Output Path: '+YELLOW+'{}'+RESET).format(output_path))
print(('Unity Log Path: '+YELLOW+'{}'+RESET).format(log_path))

monitor = True
if shutil.which('tail') is None:
    print(YELLOW+"Warning: 'tail' command is not found, log monitoring disabled.")
    monitor = False

if pathlib.Path(log_path).exists():
    os.remove(log_path)

print('Building Binary...')

# unity_process = subprocess.Popen([unity_path, '-batchmode', '-projectPath', str(pathlib.Path('./').absolute()), '-executeMethod', 'BatchBuilder.Build', '-nographics', '-quit', '-logFile', '-buildTarget' ,'Linux64', '-stackTraceLogType', 'None', '-silent-crashes'], stdout=subprocess.PIPE)
unity_process = subprocess.Popen([unity_path, '-batchmode', '-projectPath', str(pathlib.Path(__file__).parent), '-buildWindows64Player', output_path,
                                  '-nographics', '-quit', '-logFile', log_path, '-buildTarget', 'Win64', '-stackTraceLogType', 'None', '-silent-crashes'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

if monitor:
    tail_process = subprocess.Popen(
        ['tail', '-F', log_path], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL, creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)

    print("Unity Log Monitor:")
    queue = deque([])
    while True:
        line = tail_process.stdout.readline().rstrip().decode()

        if len(line) > 0:
            term_width = shutil.get_terminal_size()[0]
            queue.append(line[:(term_width-20)])
            if len(queue) > MON_HEIGHT:
                queue.popleft()

            for i in range(len(queue)):
                print("\033[2K    "+GRAY+queue[i]+RESET +
                      ("..." if len(queue[i]) >= (term_width-20) else ""))
            sys.stdout.write("\033["+str(len(queue))+"F")

        if unity_process.poll() is not None:
            break

    tail_process.send_signal(signal.CTRL_BREAK_EVENT)

print("\033[{}E".format(MON_HEIGHT), end='')

print("Unity Exited with code {code}.".format(code=unity_process.returncode))
if unity_process.returncode == 0:
    print(GREEN+"Build Succeeded!"+RESET)
else:
    print(RED+"Build Failed!"+RESET)
